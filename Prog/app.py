# Problem 6 : Solving NN with basic Mathemetics. 
from DataReading import dataR
from NNAchitechture import NNA_COSTE, NNA_MCE
import numpy as np

def sigmoid(x):
    return 1/(1+np.exp(-x))
    
def main(mystery_flower):
    
    # STEP 1: Data Reading from excel:
    filename = 'Data.csv'
    data = dataR(filename)    
    print('FULL DATA: ', data) #printing the full data
    print('FIRST ROW DATA: ', data[0]) #printing the first row data
    print('SECOND ROW LENGTH DATA: ', data[1][0]) #printing the first row data
    
    # STEP 2: NN Achitechture: 
    w1, w2, b = NNA_COSTE(data)
    print('COSTE: ', sigmoid(mystery_flower[0]*w1+mystery_flower[1]*w2+b))
    
    
if __name__ == '__main__':
    mystery_flower = [4.5, 1]
    main(mystery_flower)
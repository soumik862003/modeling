# -*- coding: utf-8 -*-
# Network
#    O      flower Type
#   / \     Weight (w1, w2) and Bias (b)
#  0   0    Length, Width

import numpy as np
from matplotlib import pyplot as plt

def sigmoid(x):
    return 1/(1+np.exp(-x))
    
def sigmoid_p(x):
    # derivative of Sigmoid function:
    return sigmoid(x) * (1-sigmoid(x))
    
def wt_bias():
    w1 = np.random.randn()
    w2 = np.random.randn()
    b = np.random.randn()
    return w1, w2, b
    
def NNA_COSTE(data):
    w1, w2, b = wt_bias()
    
    #draw the sigmoid function STEP 3: 
    X = np.linspace(-5,5,200)
    Y = sigmoid(X)
    Y_P = sigmoid_p(X)
    
    plt.plot(X,Y,'r')
    plt.plot(X,Y_P,'b') 
    plt.show()
    
    #Plotting the data in a 2D Plane:
    for i in range(0,len(data)):
        pointP = data[i]
        if pointP[2] == 1:
            color = 'r'
        else:
            color = 'b'
        plt.scatter(pointP[0], pointP[1],c=color)
    plt.grid()
    plt.show()
    
    learning_rate = 0.2       
    #Run in a loop STEP 4:
    for i in range(0,500000):
        
        # Pick a random point in the data set
        ri = np.random.randint(0,len(data))
        pointi = data[ri]
        
        # generating the output for multi variate linear regression. 
        z = pointi[0] * w1 + pointi[1] * w2 + b
        
        # Squashing the value from 0 to 1. using sigmoid
        prediction = sigmoid(z)
        
        # Calculating the error:
        target = pointi[2]
        cost = np.square(prediction - target)
        
        # derivative of the cost:
        dcost_dpred = 2 * (prediction - target)
        
        dpred_dz = sigmoid(z) * (1-sigmoid(z))
    
        dz_dw1 = pointi[0]
        dz_dw2 = pointi[1]
        dz_db = 1
    
        # Drawing the vector and minimizing the error
        dcost_dw1 = dcost_dpred * dpred_dz * dz_dw1
        dcost_dw2 = dcost_dpred * dpred_dz * dz_dw2
        dcost_db =  dcost_dpred * dpred_dz * dz_db
    
        w1 -= learning_rate * dcost_dw1
        w2 -= learning_rate * dcost_dw2
        b -= learning_rate * dcost_db
        
        if (i % 100000 == 0):
            print(cost)
            
            
    return w1, w2, b
        
def NNA_MCE(data):
    w1, w2, b = wt_bias()  
    costA =[]
    learning_rate = 0.2 
    for i in range(0,500):
        cost = 0
        for j in range(0,len(data)):
            z = data[j][0] * w1 + data[j][1] * w2 + b  
            prediction = sigmoid(z)
            target = data[j][2]
            cost = cost + np.square(prediction - target)
        cost = cost/len(data)
        costA.append(cost)
    plt.plot(costA) 
    plt.show()
     

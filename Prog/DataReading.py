# -*- coding: utf-8 -*-

import pandas as pd

def dataR(filename):
    df = pd.read_csv(filename)
    return df.to_numpy()